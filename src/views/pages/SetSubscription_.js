import React from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Row,
  Col,
} from "reactstrap";
import NormalNavbar from "components/Navbars/NormalNavbar.js";
import "./Settings.scss";
import { Auth } from "aws-amplify";


class SetSubscription extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      error: null,
      errorMsg: null,
      verificationCode: '',
      disabled: true,
      showDetail: false,
      picture: null,
      info: {
        name: '',
        phone_number: '',
        email: '',
        address: '',
        planStartDate: "2021-01-13",
        planEndDate: "2022-01-13",
        plan: "Mesh 0Distance Premium",
        "custom:planStartDate": "2021-01-13",
        "custom:planEndDate": "2022-01-13",
        "custom:plan": "Mesh 0Distance Premium",
      },
    };

    this.userInfo = this.userInfo.bind(this);
    // this.onChange = this.onChange.bind(this);
    this.onFormValueChange = this.onFormValueChange.bind(this);
    // this.handleEditing = this.handleEditing.bind(this);
  }
  async getUserInfo() {
    let user = await Auth.currentAuthenticatedUser();
    this.setState({ info: user.attributes });
  }
  componentDidMount() {
    this.getUserInfo();
  }

  async userInfo(e) {
    e.preventDefault();
    try {
      console.log(this.state.info)
      let user = await Auth.currentAuthenticatedUser();
      // await Auth.updateUserAttributes(user, this.state.info);
      await Auth.updateUserAttributes(user, {
        'custom:plan': this.state.info.plan,
        'custom:planStartDate': this.state.info.planStartDate,
        'custom:planEndDate': this.state.info.planEndDate,
      });
      window.location.href = window.location.href;
    } catch (err) {
      if (err.message != null) {
        alert(err.message);
      }
    }
  }

  onFormValueChange(e) {
    console.log(e.target.name);
    console.log(e.target.value);
    this.setState({ ...this.state, info: { [e.target.name]: e.target.value } });
  }

  render() {
    return (
      <div style={{ minHeight: "10%" }}>
        <span
          className="mask"
          style={{ height: "30%", backgroundColor: "#11cdef" }}
        />
        <NormalNavbar
          {...this.props}
          // brandText={this.getBrandText(this.props.location.pathname)}
        />
        <div className="settings-container row justify-content-center">
          <Card className="main-card">
            <Form onSubmit={this.userInfo}>
              <CardHeader>
                <Row className="align-items-center">
                  <Col xs="8">
                    <h2 className="mb-0">Subscription</h2>
                  </Col>
                  <Col className="text-right" xs="4">
                    <Button color="success" size="sm" type="submit">
                      Save
                    </Button>
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                <h6 className="heading-small text-muted mb-4">
                  Set Subscription information
                </h6>
                <div className="pl-lg-4">
                  <Row>
                    <Col lg="12">
                      <FormGroup>
                        <label className="form-control-label">
                          Mesh Drive
                        </label>
                        <Input
                          id="plan"
                          placeholder="Subscription"
                          type="text"
                          name="custom:plan"
                          value={this.state.info["custom:plan"]}
                          onChange={this.onFormValueChange}
                        />
                      </FormGroup>
                    </Col>
                    <Col lg="6">
                      <FormGroup>
                        <label className="form-control-label">Start Date</label>
                        <Input
                          id="planStartDate"
                          placeholder="Start Date"
                          type="date"
                          name="custom:planStartDate"
                          value={this.state.info["custom:planStartDate"]}
                          onChange={this.onFormValueChange}
                        />
                      </FormGroup>
                    </Col>
                    <Col lg="6">
                      <FormGroup>
                        <label className="form-control-label">End Date</label>
                        <Input
                          id="planEndDate"
                          placeholder="End Date"
                          type="date"
                          name="custom:planEndDate"
                          value={this.state.info["custom:planEndDate"]}
                          onChange={this.onFormValueChange}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                </div>
              </CardBody>
            </Form>
          </Card>
        </div>
      </div>
    );
  }
}

export default SetSubscription;
