import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Row,
  Col,
} from "reactstrap";
import NormalNavbar from "components/Navbars/NormalNavbar.js";
import "./Settings.scss";
import { Auth } from "aws-amplify";

async function UpdateData(data) {
  try {
    let user = await Auth.currentAuthenticatedUser();
    // await Auth.updateUserAttributes(user, this.state.info);
    await Auth.updateUserAttributes(user, {
      "custom:plan": data.plan,
      "custom:planStartDate": data.planStartDate,
      "custom:planEndDate": data.planEndDate,
    });
    window.location.href = window.location.href;
  } catch (err) {
    if (err.message != null) {
      alert(err.message);
    }
  }
}

export default function ScheduleMeeting(props) {
  const [info, setInfo] = useState();
  const [formData, setFormInfo] = useState({
    plan: "",
    planStartDate: "",
    planEndDate: "",
  });

  useEffect(() => {
    const getUserInfo = async () => {
      const user = await Auth.currentAuthenticatedUser();
      setInfo(user.attributes);
      console.log(user.attributes);
      console.log(info);
    };
    getUserInfo();
  }, []);

  function onFormValueChange(e) {
    console.log(e.target.name);
    console.log(e.target.value);
    setFormInfo((state) => ({ ...state, [e.target.name]: e.target.value }));
  }

  function updateUserInfo(e) {
    e.preventDefault();

    try {
      UpdateData(formData);
      window.location.href = window.location.href;
    } catch (err) {
      if (err.message != null) {
        alert(err.message);
      }
    }
  }

  return (
    <div style={{ minHeight: "10%" }}>
      <span
        className="mask"
        style={{ height: "30%", backgroundColor: "#11cdef" }}
      />
      <NormalNavbar
        {...props}
        // brandText={this.getBrandText(this.props.location.pathname)}
      />
      <div className="settings-container row justify-content-center">
        <Card className="main-card">
          <Form onSubmit={updateUserInfo}>
            <CardHeader>
              <Row className="align-items-center">
                <Col xs="8">
                  <h2 className="mb-0">Schedule a Meeting</h2>
                </Col>
                <Col className="text-right" xs="4">
                  <Button color="success" size="sm" type="submit">
                    Schedule
                  </Button>
                </Col>
              </Row>
            </CardHeader>
            <CardBody>
              <div className="pl-lg-4">
                <Row>
                  <Col lg="12">
                    <FormGroup>
                      <label className="form-control-label">Topic</label>
                      <Input
                        id="plan"
                        placeholder="Subscription"
                        type="text"
                        name="plan"
                        // value={info["custom:plan"]}
                        onChange={onFormValueChange}
                      />
                    </FormGroup>
                  </Col>

                  <CardBody>
                    <div className="pl-lg-4">
                      <Row>
                        <Col lg="12">
                          <FormGroup>
                            <label className="form-control-label">Topic</label>
                            <Input
                              id="custom:meetingTopic"
                              placeholder=""
                              type="text"
                              name="custom:meetingTopic"
                              onChange={onFormValueChange}
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label className="form-control-label">Date</label>
                            <Input
                              id="custom:meetingStartDate"
                              type="date"
                              name="custom:meetingStartDate"
                              onChange={onFormValueChange}
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label className="form-control-label">Time</label>
                            <div className="d-flex align-items-center">
                              <Input
                                id="custom:meetingStartTime"
                                type="time"
                                name="custom:meetingStartTime"
                                onChange={onFormValueChange}
                              />
                              {/* </FormGroup>
                      <FormGroup> */}
                              <label className="form-control-label px-2">
                                to
                              </label>
                              <Input
                                id="custom:meetingEndTime"
                                type="time"
                                name="custom:meetingEndTime"
                                onChange={onFormValueChange}
                              />
                            </div>
                          </FormGroup>
                        </Col>
                      </Row>
                    </div>
                  </CardBody>
                </Row>
              </div>
            </CardBody>
          </Form>
        </Card>
      </div>
    </div>
  );
}
