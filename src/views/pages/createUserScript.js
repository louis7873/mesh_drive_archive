
import { Auth } from 'aws-amplify';

let i = 0;
let errnum = 0;
async function verify(email, password) {
  console.log("try " + i);
  try {
    await Auth.confirmSignUp(email, password);
  } catch (err) {
    errnum = errnum + 1;
    console.log(email);
    console.log(JSON.stringify(err));
  }
  i = i + 1;
}

async function create(email, password) {
  console.log("try " + i);
  try {
    await Auth.signUp(email, password);
  } catch (err) {
    errnum = errnum + 1;
    console.log(email);
    console.log(JSON.stringify(err));
  }
  i = i + 1;
}

async function modify(uname, pword, name, phone_number, email, address, country, city) {
  i += 1;
  let info = {
    name,
    "phone_number": "+852" + phone_number,
    email,
    address,
    "custom:country": country,
    "custom:city": city
  };
  try {
    console.log("try " + i);
    await Auth.signIn(uname, pword);
    console.log(uname + " logged in");
    let user = await Auth.currentAuthenticatedUser();
    await Auth.updateUserAttributes(
      user, info
    );
    console.log(uname + " updated");
    await Auth.signOut();
    console.log(uname + " logged out");
  } catch (err) {
    console.log(uname + " failed");
    errnum += 1;
  }
}

async function modifyProfile_() {
  // await verify("kadimaconsultant@gmail.com", "134236");
  // await verify("cndpdcl@gmail.com", "552207");

  // console.log(i + "/" + errnum);
  i = 0; errnum = 0;
  // await modify("kadimaconsultant@gmail.com", "Ace13579", "CND Precision Development Company Limited ", "52196203", "kadimaconsultant@gmail.com", "Flat 17-18, 10/F, Landmark North, 39 Lung Sum Ave, Sheung Shui, NT", "HKG", "Hong Kong");
  // await modify("cndpdcl@gmail.com", "Ace13579", "Kadima Consultant Company", "93343936", "cndpdcl@gmail.com", "Room 128A, 1/F, Cheung Sha Wan Plaza, 833 Cheung Sha Wan Road, KL", "HKG", "Hong Kong");

  // await modify("denonwealth@gmail.com", "Ace13579", "DENON WEALTH GROUP LIMITED", "65384598", "denonwealth@gmail.com", "Flat/RM 142, 1/F, Man Po Building, 2 Tsing Hoi Circuit, Tuen Mun", "HKG", "Hong Kong");
  // await modify("fhcfhc888@gmail.com", "Ace13579", "FHC Company", "56868533", "fhcfhc888@gmail.com", "FLAT/RM 707, 7/F, LEE ON INDUSTRIAL BUILDING, 70 HUNG TO ROAD, KWUN TONG, KL", "HKG", "Hong Kong");
  // await modify("wangkeeinteriordecoration@gmail.com", "Ace13579", "Wang Kee Interior Decoration Limited", "65999547", "wangkeeinteriordecoration@gmail.com", "Unit A2, 18/F, Office Plus @ Mong Kok, 998 Canton Road , kowloon", "HKG", "Hong Kong");
  // await modify("kintotrading@gmail.com", "Ace13579", "Kinto Trading Company Limited", "67164980", "kintotrading@gmail.com", "SHOP 123A EPLAZA LEGEND TOWER 7 SHING YIP STREET KWUN TONG KL", "HKG", "Hong Kong");
  // await modify("summitgroupeng@gmail.com", "Ace13579", "Summit Group Engineering Company Limited", "67522564", "summitgroupeng@gmail.com", "Flat 2807, 28/F, Laws Commercial Plaza, 788 Cheung Sha Wan Road, Cheng Sha Wan, KL", "HKG", "Hong Kong");
  // await modify("qinmusic2008@gmail.com", "Ace13579", "Qin Music Company", "67154957", "qinmusic2008@gmail.com", "2/f, No 12, Lan Kwai Fong, Central, Hong Kong", "HKG", "Hong Kong");
  // await modify("100gigcases@gmail.com", "Ace13579", "Hundreds GIG Cases", "67179986", "100gigcases@gmail.com", "九龍灣宏開道16號德福大廈1205室", "HKG", "Hong Kong");
  // await modify("matrixconsultantscoltd@gmail.com", "Ace13579", "Matrix Consultants Company", "67540528", "matrixconsultantscoltd@gmail.com", "九龍灣宏泰道3-5號合力工業中心A座801室", "HKG", "Hong Kong");
  // await modify("yuetunghealth@gmail.com", "Ace13579", "Yue Tung Health Management Service Ltd.", "67140429", "yuetunghealth@gmail.com", "Flat H, 9/F, Houston Industrial Building, Tsuen Wan, NT", "HKG", "Hong Kong");
  // await modify("heartbbeyond@gmail.com", "Ace13579", "HEARTBEAT BEYOND FASHION", "64310234", "heartbbeyond@gmail.com", "Flat/Rm 12, BLK A, 19/F, New Trade Plaza, 6 On Ping Street, Shatin", "HKG", "Hong Kong");
  // await modify("gpcl2016@hotmail.com", "Ace13579", "Global Property Company Limited", "61886108", "gpcl2016@hotmail.com", "RM 1302 13/F, CHAOS INDUSTRIAL BUILDING, 9 KIN FAT STREET, TUEN MUN NT", "HKG", "Hong Kong");
  // await modify("hosonfungh08@gmail.com", "Ace13579", "HOSONFUNG TRADING COMPANY", "60373521", "hosonfungh08@gmail.com", "SHOP 122B PODIUM 97 BROADWAY STREET MEI FOO SUN CHUEN KL", "HKG", "Hong Kong");
  // await modify("alterdatatech@gmail.com", "Ace13579", "Alterdata Technologies Limited", "61524596", "alterdatatech@gmail.com", "Room 1404B, 14/F, Sino Centre, 582-592 Nathan Road, Mongkok, KL", "HKG", "Hong Kong");

  // await modify("ghkmbayltd@gmail.com", "Ace13579", "GHKM Bay Limited", "97222505", "ghkmbayltd@gmail.com", "Room 1303, Righteous Centre, 585 Nathan Road, Mong Kok, Hong Kong", "HKG", "Hong Kong");
  // await modify("info@starbus.com.hk", "Ace13579", "Star Business Services Limited", "68881733", "info@starbus.com.hk", "Unit C, 6/F World Trust Tower, 50 Stanley Street, Central, Hong Kong", "HKG", "Hong Kong");
  // await modify("lancialimited@outlook.com", "Ace13579", "LANCIA LIMITED", "67473860", "lancialimited@outlook.com", "LG/F, FLAT A, 25 KIN WA STREET, NORTH POINT, HK", "HKG", "Hong Kong");
  // await modify("crystalicecpy@gmail.com", "Ace13579", "Onber Reflexology Limited", "60902598", "crystalicecpy@gmail.com", "SHOP 150-151A, PODIUM STAGE 3, MEI FOO SUN CHUEN 41-43, BROADWAY ST, KL", "HKG", "Hong Kong");
  // await modify("skytransportationltd@outlook.com", "Ace13579", "Sky Transportation Limited", "69390734", "skytransportationltd@outlook.com", "FLAT/RM 21, BLK B, 5/F, SHEUNG SHUI PLAZA, 3 KA FU CLOSE, SHEUNG SHUI, NT", "HKG", "Hong Kong");
  // await modify("richadvancecorporate@outlook.com", "Ace13579", "Rich Advance Corporate Limited", "54292903", "richadvancecorporate@outlook.com", "FLAT/RM 12, BLK C, 1/F, KING YIP FACTORY, 59 KING YIP STREET, KWUN TONG, KL", "HKG", "Hong Kong");
  // await modify("chasinglifesportsclub@outlook.com", "Ace13579", "Chasing Life Sports Club and Creation Company Limited", "", "chasinglifesportsclub@outlook.com", "", "HKG", "Hong Kong");
  // await modify("ghairspasalon@outlook.com", "Ace13579", "G Hair Spa Salon Limited", "53935862", "ghairspasalon@outlook.com", "FLAT/RM 6, 25/F, ONE MIDTOWN, 11 HOT SHING ROAD, TSUEN WAN, NT", "HKG", "Hong Kong");
  // await modify("simonlee_02@hotmail.com", "Ace13579", "Chak Hin Decoration Design Engineering Limited", "98343909", "simonlee_02@hotmail.com", "1212, 12/F, 6 (On Chiu House), Cheung On Estate, Tsing Yi, NT, Hong Kong", "HKG", "Hong Kong");
  // await modify("thaithaibuddha@gmail.com", "Ace13579", "THAI HIN THAI BUDDHA AGENT", "62329893", "thaithaibuddha@gmail.com", "九龍長沙灣長順街20號時豐中心", "HKG", "Hong Kong");
  // await modify("vivoacademymusic@gmail.com", "Ace13579", "VIVO ACADEMY OF MUSIC", "54263724", "vivoacademymusic@gmail.com", "FLAT A, 1/F, KWAN SHING BUILDING, 160 TSUEN WAN MARKET STREET, TSUEN WAN, NT", "HKG", "Hong Kong");
  // await modify("kclogisticsinterltd@gmail.com", "Ace13579", "KC LOGISTICS INTERNATIONAL LIMITED", "59841779", "kclogisticsinterltd@gmail.com", "RM 404, 4/F BLK B, WAH TAK IND, BLDG 8, WAH SHING ST, KWAI CHUNG, NT", "HKG", "Hong Kong");
  // await modify("cascowayltd@gmail.com", "Ace13579", "CASCO WAY (HONG KONG) LIMITED", "68556674", "cascowayltd@gmail.com", "BLK A, 5/F, ON LOK IND BLDG, 88-90 KOWLOON CITY ROAD, TOKWAWAN", "HKG", "Hong Kong");
  // await modify("HPHPHPinvest@gmail.com", "Ace13579", "HARVEST PROPERTY INVESTMENT COMPANY LIMITED", "52485091", "HPHPHPinvest@gmail.com", "FLAT/RM 1001-1002, 10/F, DAWNING HOUSE, 145 CONNAUGHT ROAD, CENTRAL, HK", "HKG", "Hong Kong");
  // await modify("yathingtradingcpy@gmail.com", "Ace13579", "YAT HING TRADING COMPANY", "52220472", "yathingtradingcpy@gmail.com", "G/F, WAH SUN BUILDING, 10 YIN CHONG STREET, MONG KOK, KOWLOON", "HKG", "Hong Kong");
  // await modify("moonooncompany@gmail.com", "Ace13579", "MOONOON COMPANY", "91583169", "moonooncompany@gmail.com", "RM 19, 3/F, EAST SUN INDUSTRIAL CENTRE, 16 SHING YIP STREET, KWUN TONG, KOWLOON", "HKG", "Hong Kong");
  // await modify("innergyinteriordesign@gmail.com", "Ace13579", "INNERGY INTERIOR DESIGN STUDIO", "91560682", "innergyinteriordesign@gmail.com", "FLAT 13Q, 13/F, INTERNATIONAL INDUSTRIAL CENTRE, 2-8 KWEI TEI STREET, SHA TIN, NT", "HKG", "Hong Kong");
  // await modify("saikungrickyhut00@gmail.com", "Ace13579", "SAI KUNG RICKY HUT", "60578372", "saikungrickyhut00@gmail.com", "REAR PORTION G/F, NO.43A SAI KUNG MAIN STREET, SAI KUNG, NT", "HKG", "Hong Kong");
  // await modify("ACpany@gmail.com", "Ace13579", "ACCE COMPANY LIMITED", "52241863", "ACpany@gmail.com", "UNIT 7M, 3/F, BLK B, HONG KONG INDUSTRIAL CENTRE, 489-491 CASTLE PEAK ROAD, KOWLOON", "HKG", "Hong Kong");
  // await modify("leemingcom3@gmail.com", "Ace13579", "LEE MING CLINIC OF MASCULINOLOGY", "64204973", "leemingcom3@gmail.com", "九龍彌敦道375號金勳大廈7/F(8字樓)三室", "HKG", "Hong Kong");
  // await modify("emineworlde05@gmail.com", "Ace13579", "EMINE WORLD LIMITED", "98720547", "emineworlde05@gmail.com", "FLAT/RM A7, BLK E, 1/F, WANG KWONG INDUSTRIAL BUILDING, 45 HUNG TO ROAD, KWUN TONG, KL", "HKG", "Hong Kong");
  // await modify("billicreation@gmail.com", "Ace13579", "Billion Creation Global Company", "64898435", "billicreation@gmail.com", "2, TAI PO TIN, TA KWU LING, NEW TERRITORIES", "HKG", "Hong Kong");
  // await modify("virgooo2018@gmail.com", "Ace13579", "VIRGO", "91472941", "virgooo2018@gmail.com", "FLAT 807 & 810, KWAI ON FTY EST, 103-113 TAI LIN PAI ROAD, KWAI CHUNG, NT", "HKG", "Hong Kong");
  // await modify("sapient2099@gmail.com", "Ace13579", "SAPIENT CONSULTANTS", "62245175", "sapient2099@gmail.com", "FLAT/RM D, BLK 3, 1/F, SCENEWAY GARDEN, LAM TIN, KL", "HKG", "Hong Kong");
  // await modify("fusionmeatlf03@gmail.com", "Ace13579", "FUSION METALS LIMITED", "67504316", "fusionmeatlf03@gmail.com", "FLAT/RM 1205A 12/F WING ON PLAZA 62 MODY ROAD TSIMSHATSUI", "HKG", "Hong Kong");
  // await modify("ccwaterproof@gmail.com", "Ace13579", "Chiu Chun Waterproofing & Engineering Limited", "93336945", "ccwaterproof@gmail.com", "FLAT/RM 14, BLK A, 11/F, HANG WAI IND CTR, 6 KIN TAI ST, TUEN MUN, NT", "HKG", "Hong Kong");
  // await modify("yuncheehop@gmail.com", "Ace13579", "YUN CHEE HOP LIMITED", "59122787", "yuncheehop@gmail.com", "DD123 LOT 629, FUK HI STREET, WANG CHAU, NT", "HKG", "Hong Kong");
  // await modify("onlyone20211314@hotmail.com", "Ace13579", "ONLY ONE TREATMENT CENTER LIMITED", "61793384", "onlyone20211314@hotmail.com", "1/F, SHOP 108B, FORTUNE PLAZA, 4 ON CHEE ROAD, TAI PO, NT", "HKG", "Hong Kong");
  // await modify("wchlimit@gmail.com", "Ace13579", "WING CITY HOLDING LIMITED", "51323614", "wchlimit@gmail.com", "FLAT 8, 4/F, VICTORY INDUSTRIAL BUILDING, 151-157 WO YI HOP ROAD, KWAI CHUNG, NT", "HKG", "Hong Kong");
  // await modify("w01wellelem@hotmail.com", "Ace13579", "WELL ELEMENTS LIMITED", "59121712", "w01wellelem@hotmail.com", "RM 12A, 12/F, WAY ON COMMERCIAL BUILDING, 500 JAFFE ROAD, CAUSEWAY BAY, HONG KONG", "HKG", "Hong Kong");
  // await modify("s06sunyat@gmail.com", "Ace13579", "SUN YAT SEN CENTRE", "61541198", "s06sunyat@gmail.com", "NO.44 WANG SHAN KEUK SAN TSUEN, MA MEI HA SHA TAU KOK ROAD, FANLING, NT", "HKG", "Hong Kong");
  // await modify("eastftf@gmail.com", "Ace13579", "EASY FAST FORWARDING LIMITED", "55746671", "eastftf@gmail.com", "FLAT/RM 2, G/F, 442 DES VOEUX ROAD WEST, SAI WAN, HK", "HKG", "Hong Kong");
  console.log(i + "/" + errnum);
}

export default async function run() {
  modifyProfile_();
  // await verify("denonwealth@gmail.com","276515");
  // await verify("fhcfhc888@gmail.com","540695");
  // await verify("wangkeeinteriordecoration@gmail.com","699496");
  // await verify("kintotrading@gmail.com","991796");
  // await verify("summitgroupeng@gmail.com","629412");
  // await verify("qinmusic2008@gmail.com","584777");
  // await verify("100gigcases@gmail.com","504062");
  // await verify("matrixconsultantscoltd@gmail.com","634960");
  // await verify("yuetunghealth@gmail.com","796688");
  // await verify("heartbbeyond@gmail.com","495780");
  // await verify("gpcl2016@hotmail.com","741475");
  // await verify("hosonfungh08@gmail.com","451259");
  // await verify("alterdatatech@gmail.com","650708");


  // await create("ghkmbayltd@gmail.com", "201508");
  // await create("info@starbus.com.hk", "Ace13579");
  // await create("lancialimited@outlook.com", "831358");
  // await create("onberreflexology@outlook.com", "156933");
  // await create("crystalicecpy@gmail.com", "Ace13579");
  // await create("skytransportationltd@outlook.com", "382797");
  // await create("richadvancecorporate@outlook.com", "89496");
  // await create("chasinglifesportsclub@outlook.com", "485317");
  // await create("ghairspasalon@outlook.com", "968751");
  // await create("simonlee_02@hotmail.com", "157513");
  // await create("kwaifung2000@yahoo.com", "Ace13579");
  // await create("thaithaibuddha@gmail.com", "900814");
  // await create("vivoacademymusic@gmail.com", "810827");
  // await create("kclogisticsinterltd@gmail.com", "507110");
  // await create("cascowayltd@gmail.com", "812218");
  // await create("HPHPHPinvest@gmail.com", "155094");
  // await create("elitelearncentre@gmail.com", "Ace13579");
  // await create("yathingtradingcpy@gmail.com", "834365");
  // await create("moonooncompany@gmail.com", "336970");
  // await create("innergyinteriordesign@gmail.com", "520315");
  // await create("saikungrickyhut00@gmail.com", "381567");
  // await create("goldenpetpetser@gmail.com", "Ace13579");
  // await create("ACpany@gmail.com", "901724");
  // await create("ddbnbltd@gmail.com", "Ace13579");
  // await create("eastftf@gmail.com", "670023");
  // await create("s06sunyat@gmail.com", "534937");
  // await create("w01wellelem@hotmail.com", "621489");
  // await create("wchlimit@gmail.com", "497896");
  // await create("onlyone20211314@hotmail.com", "186616");
  // await create("yuncheehop@gmail.com", "Ace13579");
  // await create("ccwaterproof@gmail.com", "Ace13579");
  // await create("fusionmeatlf03@gmail.com", "Ace13579");
  // await create("sapient2099@gmail.com", "730022");
  // await create("virgooo2018@gmail.com", "672939");
  // await create("billicreation@gmail.com", "285109");
  // await create("emineworlde05@gmail.com", "635410");
  // await create("leemingcom3@gmail.com", "537215");
  // await create("info@hkaic.com", "62505");
}