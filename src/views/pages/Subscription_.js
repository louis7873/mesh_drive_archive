import React from "react";
import { Card, CardHeader, CardBody, Row, Col } from "reactstrap";

import moment from "moment";
import NormalNavbar from "components/Navbars/NormalNavbar.js";
import { Auth } from "aws-amplify";


class Subscription extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      error: null,
      errorMsg: null,
      verificationCode: "",
      disabled: true,
      showDetail: false,
      picture: null,
      info: {
        name: "",
        phone_number: "",
        email: "",
        address: "",
        "custom:planStartDate": "",
        "custom:planEndDate": "",
        "custom:plan": "",
      },
    };
    this.info = this.info.bind(this);
    this.userInfo = this.userInfo.bind(this);
  }
  async getUserInfo() {
    let user = await Auth.currentAuthenticatedUser();
    this.setState({ info: user.attributes });
  }

  componentDidMount() {
    this.getUserInfo();
  }

  render() {
    return (
      <div style={{ minHeight: "10%" }}>
        <span
          className="mask"
          style={{ height: "30%", backgroundColor: "#11cdef" }}
        />
        <NormalNavbar {...this.props} />
        <div className="settings-container row justify-content-center">
          <Card className="main-card">
            <CardHeader>
              <Row className="align-items-center">
                <Col xs="8">
                  <h2 className="mb-0">Subscription</h2>
                </Col>
              </Row>
            </CardHeader>
            <CardBody>
              <div>Company Name</div>
              <h2>{this.state.info.name}</h2>

              <div>Email</div>
              <h2>{this.state.info.email}</h2>
              <hr />

              <div>Plan</div>
              <h2>
                {this.state.info["custom:plan"]}
              </h2>

              <div>Subscription Period</div>
              <h2>
                {moment(this.state.info["custom:planStartDate"]).format(
                  "D MMMM YYYY"
                )}{" "}
                to{" "}
                {moment(this.state.info["custom:planEndDate"]).format(
                  "D MMMM YYYY"
                )}
              </h2>
            </CardBody>
          </Card>
        </div>
      </div>
    );
  }
}

export default Subscription;
