import React, { useEffect, useState } from "react";
import { Card, CardHeader, CardBody, Row, Col } from "reactstrap";

import moment from "moment";
import NormalNavbar from "components/Navbars/NormalNavbar.js";
import { Auth } from "aws-amplify";

export default function Subscription(props) {
  const [isLoading, setIsLoading] = useState(false);

  const [user, setUser] = useState(null);
  useEffect(() => {
    // Access the user session on the client
    Auth.currentAuthenticatedUser()
      .then((user) => {
        console.log("User: ", user);
        setUser(user);
        setIsLoading(true);
      })
      .catch((err) => setUser(null));
  }, []);

  if (!isLoading) {
    return <>Loading...</>;
  }

  const plan = user.attributes["custom:plan"];
  const start = user.attributes["custom:planStartDate"];
  const end = user.attributes["custom:planEndDate"];
  const today = new Date();
  console.log(today)

  return (
    <>
      <div style={{ minHeight: "10%" }}>
        <span
          className="mask"
          style={{ height: "30%", backgroundColor: "#11cdef" }}
        />
        <NormalNavbar {...props} />
        <div className="settings-container row justify-content-center">
          <Card className="main-card">
            <CardHeader>
              <Row className="align-items-center">
                <Col xs="8">
                  <h2 className="mb-0">Subscription </h2>
                </Col>
              </Row>
            </CardHeader>
            <CardBody>
              <div>Company Name</div>
              <h2>{user.attributes["name"]}</h2>

              <div>Email</div>
              <h2>{user.attributes["email"]}</h2>
              <hr />

              <div>Plan</div>
              <h2>{plan ? plan : "Mesh 0Distance Premium"}</h2>

              <div>Subscription Period</div>
              <div class="d-flex align-items-start">
                <h2>
                  {start
                    ? moment(start).format("D MMMM YYYY")
                    : moment("13-1-2021").format("D MMMM YYYY")}{" "}
                  to{" "}
                  {start
                    ? moment(end).format("D MMMM YYYY")
                    : moment("13-1-2022").format("D MMMM YYYY")}
                </h2>
                { moment(end).isBefore(today) && 
                (
                  <span className="ml-1 bg-danger px-3 text-uppercase text-white rounded">
                    Expired
                  </span>)
                }
              </div>
            </CardBody>
          </Card>
        </div>
      </div>
    </>
  );
}
