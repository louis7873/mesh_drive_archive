import React, { useEffect, useState } from "react";
import { Card, CardHeader, CardBody, Row, Col } from "reactstrap";

import moment from "moment";
import NormalNavbar from "components/Navbars/NormalNavbar.js";
import { Auth } from "aws-amplify";


export default function Subscription(){
  const [ planInfo, setPlanInfo ] = useState()


  useEffect(() => {
    const getData = async () => {
      const response = await fetch('https://mesh-drive-2e39f-default-rtdb.asia-southeast1.firebasedatabase.app/plans.json');
      const data = await response.json()
      setPlanInfo(data)
      console.log(planInfo)
    }
    getData()

  },[])

  return (
    <>
      <div>{planInfo.planTitle}</div>
    </>
  )
}
