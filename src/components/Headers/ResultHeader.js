import React from "react";
import { Redrict, Redirect } from "react-router-dom";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// reactstrap components
import {
  Breadcrumb,
  BreadcrumbItem,
  Button,
  Container,
  Row,
  Col,
  Input
} from "reactstrap";

import { FaHome } from "react-icons/fa";

class TimelineHeader extends React.Component {
  render() {
    return (
      <>
        <div className="header header-dark bg-info pb-6 content__title content__title--calendar">
          <Container fluid>
            <div className="header-body">
              <Row className="align-items-center py-4">
                <Col lg="6" xs="7">
                  {/* <h6 className="fullcalendar-title h2 text-white d-inline-block mb-0">
                    {
                      this.props.path == "" ? 
                      "Mesh Drive" : 
                      this.props.path.split("/")[this.props.path.split("/").length-1]
                    }
                  </h6>{" "} */}
                  <Breadcrumb
                    className="d-none d-md-inline-block ml-lg-4"
                    listClassName="breadcrumb-links breadcrumb-dark"
                  >
                    <BreadcrumbItem>
                      {this.props.keyword ? <span className="result">Search Result: {this.props.keyword}</span> : "Loading"}
                    </BreadcrumbItem>
                  </Breadcrumb>
                </Col>
              </Row>
            </div>
          </Container>
        </div>
      </>
    );
  }
}

TimelineHeader.propTypes = {
  name: PropTypes.string,
  parentName: PropTypes.string
};

export default TimelineHeader;
