const SUBSCRIPTIONS = [
  {
    name: "Company 1",
    email: "kakao.frodo@gmail.com",
    startDate: "2021-09-01",
    endDate: "2022-02-01"
  },
  {
    name: "Compang 2",
    email: "kakao.frodo@gmail.com",
    startDate: "2021-10-02",
    endDate: "2022-03-01"
  },
  {
    name: "Compang 3",
    email: "kakao.frodo@gmail.com",
    startDate: "2021-10-09",
    endDate: "2022-03-01"
  }
]


export function getAllSubscriptions() {
  return SUBSCRIPTIONS;
}

export function getSubscriberByEmail(email) {
  return SUBSCRIPTIONS.find((subscriber) => subscriber.email === email);
}
